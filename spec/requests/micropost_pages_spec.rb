require 'spec_helper'

describe "site pages" do

  subject { page }

  let(:user) { FactoryGirl.create(:user) }
  before { sign_in user }

  describe "site creation" do
    before { visit root_path }
    
    describe "with invalid information" do
      
      it "should not create a site" do
        expect { click_button "Post" }.not_to change(site, :count)
      end

      describe "error messages" do
        before { click_button "Post" }
        it { should have_content('error') } 
      end
    end

    describe "with valid information" do

      before { fill_in 'site_content', with: "Lorem ipsum" }
      it "should create a site" do
        expect { click_button "Post" }.to change(site, :count).by(1)
      end
    end
  end

  describe "site destruction" do
    before { FactoryGirl.create(:site, user: user) }
    
    describe "as correct user" do
      before { visit root_path }
      
      it "should delete a site" do
        expect { click_link "delete" }.to change(site, :count).by(-1)
      end
    end
  end
end