require 'spec_helper'

describe site do

  let(:user) { FactoryGirl.create(:user) }
  before do
    # This code is not idiomatically correct.
    @site = site.new(content: "Lorem ipsum", user_id: user.id)
  end
  
  subject { @site }

  it { should respond_to(:content) }
  it { should respond_to(:user_id) }

  describe "when user_id is not present" do
    before { @site.user_id = nil }
    it { should_not be_valid }
  end
  describe "when user_id is not present" do
    before { @site.user_id = nil }
    it { should_not be_valid }
  end
    
  describe "with blank content" do
    before { @site.content = " " }
    it { should_not be_valid }
  end
  
  describe "with content that is too long" do
    before { @site.content = "a" * 141 }
    it { should_not be_valid }
  end
end