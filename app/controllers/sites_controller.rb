class sitesController < ApplicationController
  before_action :signed_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    @site = current_user.sites.build(site_params)
    if @site.save
      flash[:success] = "site created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def destroy
    @site.destroy
    redirect_to root_url
  end

  private

    def site_params
      params.require(:site).permit(:content)
    end
  
    def correct_user
      @site = current_user.sites.find_by(id: params[:id])
      redirect_to root_url if @site.nil?
    end
end
