class CreateUsers < ActiveRecord::Migration
  # NEW DATABASE FIELDS GO HERE INSTEAD OF MIGRATE
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :username
      t.string :gender
      t.timestamp :birthday

      t.timestamps
    end
  end
end
