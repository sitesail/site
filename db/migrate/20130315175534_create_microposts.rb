class Createsites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :content
      t.integer :user_id

      t.timestamps
    end
    add_index :sites, [:user_id, :created_at]
  end
end
